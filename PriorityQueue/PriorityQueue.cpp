#include "PriorityQueue.h"

PriorityQueue::PriorityQueue()
{
    itemCount = 0;                                      //setter køen til null
}

void PriorityQueue::insert(string elem, int priority)
{
    Node *newNode = createNode(elem, priority);         //lager en ny node
    int pos = itemCount - 1;                            //setter siste posisjon
    heap.push_back(newNode);                            //legger til noden i køen
    while(true)
    {
        if (pos == 0)                                   //is at root
            break;

        int parent = (pos-1)/2;                         //foreldre sin posisjon
        if (heap.at(pos)->priority >= heap.at(parent)->priority)    //sjekke prioritetene til nodene. Er denne større
            break;                                                  //enn parent vil den hoppe ut av løkken
        else
        {
            Node* temp = heap.at(pos);                              //er den mindre vil den bytte plass med noden over.
            heap.at(pos) = heap.at(parent);
            heap.at(parent) = temp;
            pos = parent;
        }
    }

}

void PriorityQueue::remove()                                        //fjerner en node fra køen og setter siste node på top noden
{
    if (itemCount > 0)
    {
        string removed = heap.at(0)->elem;                          //tar vare på fjernet node sitt element
        int priorityRemove = heap.at(0)->priority;                  //og prioriteten til noden
        heap.at(0) = heap.at(itemCount - 1);
        itemCount--;
        heap.pop_back();                                            //fjerner noden

        int pos = 0;

        while (true)
        {
            int childLeft  = 2*pos + 1;                             //regner ut hvem som er barna. Valgt å ha root på null.
            int childRight = 2*pos + 2;                             //starter man på index 1 slipper man + 2 til venstre barn.

            if(childLeft >= itemCount) //out of queue
                break;

            int smallestChild;

            if(childRight >= itemCount)     //No second child
            {
                smallestChild = childLeft;
            }
            else if (heap.at(childLeft)->priority <= heap.at(childRight)->priority)  //sjekker hvem som har høyest prioritet. 1 er best
            {
                smallestChild = childLeft;
            }
            else
            {
                smallestChild = childRight;
            }

            if(heap.at(smallestChild)->priority >= heap.at(pos)->priority)           //bryter løkken om priotet er stor
            {
                break;
            }
            Node* temp;
            temp = heap.at(pos);
            heap.at(pos) = heap.at(smallestChild);                                  //noden dyttes nedover om den har en lav prioritet
            heap.at(smallestChild) = temp;

            pos = smallestChild;
        }
        cout << endl  << removed << " was removed from the Queue! Priority: " << priorityRemove << endl;
    }
}


Node *PriorityQueue::createNode(string elem, int priority)      //lager en node
{
    Node* n = new Node;

    n->elem = elem;
    n->priority = priority;
    n->position = itemCount;
    itemCount++;
    return n;
}

void PriorityQueue::printAll()          //printer ut hele køen. Iterator er en fin ting <3
{
    for (vector<Node*>::const_iterator it = heap.begin(), end = heap.end();it != end; ++it)
        cout << (*it)->elem << " " << (*it)->priority << endl;
}

