#include <iostream>
#include "PriorityQueue.h"
#include <chrono>
#include <thread>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

using namespace std;

void timer();

int addNode = 0;
int removeNode = 1000;
int wait = 0;
bool runAddNode;
string name[10] = {"Paul", "Kaya", "Steinar", "Ole", "Dag", "Rebecca", "Andre", "Adrian", "Berd", "Andreas"};

int main()
{
    PriorityQueue pq;
    pq.insert("Askeladde", 1);
    pq.insert("Peer", 3);
    pq.insert("Paal", 8);
    pq.insert("Ola", 1);
    pq.insert("Nordmann", 22);
    pq.insert("Kari",21);
    pq.insert("NordKvinne", 25);
    pq.insert("Jenni", 26);                         //Legger til litt noder siden det slettes mer enn det skapes.
    srand(time(NULL));


    while(true)     //kjører programmet i en uendelig loop for å få tiden til å gå
    {
        //Sette verdier til gjennomkjøring med løkken
        if (addNode <= 0)
        {
            addNode = rand()%3000 + 1;
        }
        if (removeNode <= 0)
            removeNode = 1000;

        //Sjekker hvem node som kommer først
        if (addNode < removeNode)
        {
            wait = addNode;
            removeNode -= addNode;      //hvor mye tid som er igjen til neste node
            runAddNode = true;          //hvem node sin tur det er
            addNode = 0;
        }
        else
        {
            wait = removeNode;
            addNode -= removeNode;
            removeNode = 0;
            runAddNode = false;
        }

        //Får programmet til å vente før det kjører videre
        auto start = std::chrono::high_resolution_clock::now();
        std::this_thread::sleep_for(std::chrono::milliseconds(wait));
        auto end = std::chrono::high_resolution_clock::now();
        std::chrono::duration<double, std::milli> elapsed = end-start;

        //Kjører hvilken node som trenges etter wait er ferdig
        if (runAddNode)                                     //legger til tilfeldige navn og prioritet
        {
            pq.insert(name[rand()%10], rand()%20+1);
        }
        if (!runAddNode)                                    //fjerner en node
        {
            if(pq.size() > 0)
            {
                cout << "Time: " << elapsed.count() << endl;
                pq.remove();
                pq.printAll();                              //printer når det fjernes en person fra køen
            }
        }
    }
    pq.printAll();
    return 0;
}


