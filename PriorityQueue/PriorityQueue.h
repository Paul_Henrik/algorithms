#ifndef PRIORITYQUEUE_H
#define PRIORITYQUEUE_H
#include <iostream>
#include <string>
#include <vector>

using namespace std;

struct Node
{
    string elem;
    int priority;
    int position;
};                                                  //Node var så liten at jeg valgte å ha den inni headeren her.

class PriorityQueue
{
public:
    PriorityQueue();                                //lager en prioritetskø
    void insert(string elem, int priority);         //legger til en node
    void remove();                                  //fjerner en node
    int size() {return itemCount;}                  //sjekker hvor stor køen er
    Node *createNode(string elem, int priority);    //lager en ny node til køen
    void printAll();                                //printer ut hele køen
private:
    vector<Node*> heap;                             //lager en vector array
    int itemCount;                                  //telling på hvor lang køen er.. mulig heap.size() funker også.
};





#endif // PRIORITYQUEUE_H
