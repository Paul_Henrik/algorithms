TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    Tree.cpp \
    ChildList.cpp

HEADERS += \
    Tree.h \
    ChildList.h

