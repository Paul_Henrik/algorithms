#ifndef NODE_H
#define NODE_H
#include <iostream>
#include <string>

using namespace std;


class Node
{
public:
    Node();
private:
    string elem;
    int x;
    int y;
    Node *childLeft;
    Node *childRight;
    friend class Tree;
};

#endif // NODE_H
