#include "Tree.h"

Tree::Tree()
{
    scene = NULL;
}

Node *Tree::createChild(string input)
{
    Node *n = new Node;
    n->elem = input;
    n->childLeft = NULL;
    n->childRight = NULL;
    n->x = 0;
    n->y = 0;
    return n;
}

void Tree::addChild(string input)
{
    addChildPrivate(input, scene);
}

void Tree::setChildPos(Node *ptr, int x, int y)
{
    ptr->x = x;
    ptr->y = y;
}


void Tree::addChildPrivate(string input, Node *ptr)
{
    if (scene == NULL)
    {
        scene = createChild(input);
    }
    else if(ptr->childLeft == NULL)
    {
        ptr->childLeft = createChild(input);
    }
    else if (ptr->childRight == NULL)
    {
        ptr->childRight = createChild(input);
    }
    else if (ptr->childRight == NULL && ptr->childLeft == NULL)
    {
        //Create a new node in a single read list here
    }

    /* else if(ptr->childLeft != NULL)
    {
        addChildPrivate(input, ptr->childLeft);     //If left child is not empty. Go left
    }
    else
        ptr->childLeft = createChild(input);

    if (ptr->childRight != NULL)
    {
        addChildPrivate(input, ptr->childRight);
    }
    else
        ptr->childRight = createChild(input);*/
}

void Tree::printTreePrivate(Node *ptr)
{
    if(scene != NULL)
    {
        if(ptr->childLeft != NULL)
        {
            printTreePrivate(ptr->childLeft);
        }
        cout << ptr->elem << " ";
        if(ptr->childRight != NULL)
        {
            printTreePrivate(ptr->childRight);
        }
    }
    else
        cout << "The tree is empty" << endl;

}


void Tree::printTree()
{
    printTreePrivate(scene);
}



