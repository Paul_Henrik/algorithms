#ifndef TREE_H
#define TREE_H
#include "Node.h"
#include <iostream>
#include <string>

using namespace std;

class Tree
{
public:
    Tree();
    void addChild(string input);
    void setChildPos(Node *ptr, int x, int y);
    void printTree();
    void makeList();
private:
    void addChildPrivate(string input, Node* ptr);
    void printTreePrivate(Node *ptr);
    Node *createChild(string input);
    Node *scene;
    Node *next;
};

#endif // TREE_H
