#include "HeapTree.h"

HeapTree::HeapTree()
{
    root = NULL;
}

void *HeapTree::addChild(int priority, string elem)
{
    Node *parent = current;
    Node  *newNode = createChild(parent,priority, elem);
    if (root == NULL)
    {
        root = createChild(NULL, priority, elem);
        current = root;
        root->height = 0;
    }
    else if (root->leftChild == NULL)
    {
        parent->leftChild = newNode;
    }
    else if (root->rightChild == NULL)
        root->rightChild = newNode;
    else if (root->leftChild != NULL)
    {
        current = parent->leftChild;
        current->leftChild = newNode;
    }

    return newNode;
}

Node *HeapTree::createChild(Node *parent, int _priority, string _elem)
{
    Node *n = new Node;
    n->elem = _elem;
    n->priority = _priority;
    n->parent = parent;
    n->leftChild = NULL;
    n->rightChild = NULL;
    if (parent != NULL)
    {
        n->height = current->height + 1;
    }
    return n;
}

void HeapTree::printAll()
{
    printAllpriv(root);
}

void HeapTree::printAllpriv(Node *ptr)
{
    if(root != NULL)
    {
        if (ptr->leftChild != NULL)
        {
            printAllpriv(ptr->leftChild);
        }
        cout << "priority: " << ptr->priority << "  Name: " << ptr->elem << " Height: " << ptr->height <<  endl;
        if (ptr->rightChild != NULL)
        {
            printAllpriv(ptr->rightChild);
        }
    }
    else
    {
        cout << "The heap is empty" << endl;
    }
}
