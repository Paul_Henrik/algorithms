#ifndef HEAPTREE_H
#define HEAPTREE_H
#include <iostream>
#include <vector>
#include <string>


using namespace std;

class Node
{
private:
    int priority;
    int height;
    string elem;
    Node *leftChild;
    Node *rightChild;
    Node *parent;
    friend class HeapTree;
};

class HeapTree
{
public:
    HeapTree();
    Node *postion(Node &_current);
    void *addChild(int priority, string elem);
    Node *createChild(Node *parent, int priority, string elem);
        void removeMin();
    void bubbleUp();
    void bubbldeDown();
    void printAll();
private:
    void printAllpriv(Node *ptr);
    Node *root;
    Node *current;
};

#endif // HEAPTREE_H
