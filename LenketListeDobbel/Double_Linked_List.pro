TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += main.cpp \
    DNode.cpp \
    DLinkedList.cpp

HEADERS += \
    DNode.h \
    DLinkedList.h

