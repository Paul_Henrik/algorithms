#ifndef DLINKEDLIST_H
#define DLINKEDLIST_H
#include "DNode.h"
#include <string>

using namespace std;

//typedef string Elem;
class DLinkedList
{
public:
    DLinkedList();
    ~DLinkedList();
    bool empty() const;
    const Elem &front() const;
    const Elem &back() const;
    void removeFront();
    void removeBack();
    void printForward();
    void printBackward();
    void add(const Elem &e);
    void remove(DNode *v);
    void search();
    void removeSpecificNode();

    class Iterator
    {
    public:
        Iterator(DNode* node) : current(node) {}

        void operator++()
        {
            current = current->next;
        }
        void operator--()
        {
            current = current->prev;
        }

        //Learned from http://en.cppreference.com/w/cpp/language/operators;

        //const makes sure the value can't be changed, friend class to be able to access private;
        friend bool operator==(const Iterator& lhs, const Iterator& rhs) //Friend gives access to the other private var. checks if current node is same as the next one.
        {
            return lhs.current == rhs.current;
        }

        friend bool operator!=(const Iterator& lhs, const Iterator& rhs) //checks it the current is not equal to the next node
        {
            return lhs.current != rhs.current;
        }
        Elem operator*() { return current->elem; } //Prints out elem

    private:
        DNode *current;
    };
    Iterator begin();
    Iterator end();
private:
    DNode *header;
    DNode *trailer;
    DNode *current;
};


#endif // DLINKEDLIST_H
