#include "DLinkedList.h"
#include <iostream>
#include <string>
using namespace std;

DLinkedList::DLinkedList()
{
    header = new DNode;     //Create sentinels
    trailer = new DNode;
    header->next = trailer;
    trailer->prev = header;
}

DLinkedList::~DLinkedList()
{
    while(!empty()) removeFront();
    delete header;
    delete trailer;
}

bool DLinkedList::empty() const
{
    return (header->next == trailer);
}

const Elem &DLinkedList::front() const
{
    return header->next->elem;
}

const Elem &DLinkedList::back() const
{
    return trailer->prev->elem;
}

void DLinkedList::removeFront()
{
    remove(header->next);
}

void DLinkedList::removeBack()
{
    remove(trailer->prev);
}

void DLinkedList::printForward()
{
    current = header->next;
    while (current != trailer)
    {
        cout << current->elem << endl;
        current = current->next;
    }
}

void DLinkedList::printBackward()
{
    current = trailer->prev;
    while (current != header)
    {
        cout << current->elem << endl;
        current = current->prev;
    }
}

void DLinkedList::add(const Elem &e)
{
    bool denied;
    DNode *nextNode = header->next;
    DNode *newNode = new DNode; newNode->elem = e;

    for (DNode *p = header->next; p != trailer; p=p->next)
    {
        if (*newNode == *p)
        {
            cout << "Item already exists" << endl;
            denied = true;
            delete newNode;
            break;
        }
        else
        denied = false;
    }
    while (newNode->elem > nextNode->elem && nextNode != trailer && !denied)
    {
        nextNode = nextNode->next;
    }
    if(!denied)
    {
    newNode->next = nextNode;
    newNode->prev = nextNode->prev;
    nextNode->prev->next = newNode;
    nextNode->prev = newNode;
    }
}

void DLinkedList::remove(DNode *v)
{
    DNode *u = v->prev;
    DNode *w = v->next;
    u->next = w;
    w->prev = u;
    delete v;
}

void DLinkedList::search()
{
    cout << "what do you want to search for: ";
    string inn;
    cin >> inn;

    current = header->next;
    while (current != trailer)
    {

        if (current->elem == inn)
        {
            cout << "Found in list" << endl;
            break;
        }
        else if (current == trailer->prev)
        {
            cout << "Not found in list" << endl;
        }
        current = current->next;
    }


}

void DLinkedList::removeSpecificNode()
{
    cout << "which item would you like to remove? ";
    string inn;
    cin >> inn;

    current = header->next;
    while (current != trailer)
    {

        if (current->elem == inn)
        {
            cout << "Found in list and removed" << endl;
            remove(current);
            break;
        }
        else if (current == trailer->prev)
        {
            cout << "Not found in list" << endl;
        }
        current = current->next;
    }
}

DLinkedList::Iterator DLinkedList::begin()
{
    Iterator it(header->next);
    return it;
}

DLinkedList::Iterator DLinkedList::end()
{
    Iterator it(trailer);
    return it;
}

