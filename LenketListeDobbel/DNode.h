#ifndef DNODE_H
#define DNODE_H
#include <string>
using namespace std;

typedef string Elem;
class DNode
{
public:
    DNode();
private:
    Elem elem;
    DNode *prev;
    DNode *next;
    bool operator==(DNode equal);
    friend class DLinkedList;
};

#endif // DNODE_H
